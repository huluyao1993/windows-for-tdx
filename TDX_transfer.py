# import argparse
# import csv
# import pandas as pd
#
# def blk_to_csv(raw_filename,new_filename):
#
#     with open(raw_filename,'r') as f:
#         stock_list = f.readlines()
#         print(stock_list)
#
#     with open(new_filename,'w') as f :
#         csv_write = csv.writer(f)
#         csv_write.writerow(stock_list)
#     return new_filename
#
# if __name__ == '__main__':
#
#     parser = argparse.ArgumentParser()  # 实例化
#     parser.add_argument('blockname')
#     args = parser.parse_args()  # 接受的参数
#     blockname = args.blockname
#
#     filepath = 'D:/TDX/T0002/blocknew/'
#     raw_filename = filepath + str(blockname) +'.blk'
#     new_filename = 'D:/TDX/T0002/blocknew/' + str(blockname) +'.csv'
#
#     print(blk_to_csv(raw_filename,new_filename))

import pandas as pd
import argparse

parser = argparse.ArgumentParser()  # 实例化
parser.add_argument('blockname')
args = parser.parse_args()  # 接受的参数
blockname = args.blockname

data = []
filepath = 'D:/TDX/T0002/blocknew/'
raw_filename = filepath + str(blockname) +'.blk'
new_filename = 'D:/TDX/T0002/blocknew/' + str(blockname) +'.csv'

# with open (new_filename,'r') as f :
f= open(raw_filename,'r')
for line in f :
    da = line.replace('\n','')
    data.append(da)
data.pop(0)
df = pd.DataFrame(data)
df.columns = ['raw_data']
df['market'] = df['raw_data'].apply(lambda s: str(s)[0])
df['code'] = df['raw_data'].apply(lambda s: str(s)[1:7])
del df.raw_data
df.to_csv(new_filename,index=False)